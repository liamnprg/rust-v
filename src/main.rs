#![feature(panic_handler)]
#![feature(global_asm)]
#![no_std]
#![no_main]

use core::panic::PanicInfo;

#[no_mangle]
pub extern "C" fn kmain() -> ! {
    serial_write("tu-V\n");
    //panic!();
    loop {}
}
#[no_mangle]
pub extern "C" fn loops() -> ! {
    loop {}
}

#[no_mangle]
pub extern "C" fn kernel_irq_handler()  {
    serial_write("Irq\n");
}
//global_asm!(include_str!("asm/main.s"));

//diosix code below
pub fn serial_write(s: &str) {
  for c in s.chars() {
    let uart_addr = 0x10000000 as *mut char;
    unsafe { *uart_addr = c; }
  }
}

#[panic_handler]
#[no_mangle]
pub fn panic(_info: &PanicInfo) -> ! {
    serial_write("panic-V\n");
    loop {}
}

#[no_mangle]
pub extern "C" fn abort() -> ! {
    serial_write("abort-V\n");
    loop {}
}

