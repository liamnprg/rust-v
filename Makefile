all:
	cd src/asm && rm -f main.o && riscv32-elf-as main.s -march rv32imafc -o main.o
	-rm -rf target/*
	RUSTFLAGS='-C link-arg=-Tsrc/asm/link.ld -C link-arg=src/asm/main.o' xargo build --target riscv32imac-unknown-none-elf --release --verbose 
od:
	riscv32-elf-objdump -D target/riscv32imac-unknown-none-elf/release/rst
q: all
	qemu-system-riscv32 -kernel target/riscv32imac-unknown-none-elf/release/rst -machine virt -nographic
